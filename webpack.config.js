const webpack = require('webpack');
const path = require('path');

const config = {
  mode: 'development',
  entry: {
    main: './src/index.js'
  },
  output: {
    filename: 'bundle.js',
    // publicPath: path.resolve(__dirname, 'dist'),
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [{
          loader: 'file-loader',
          options: {
            // publicPath: '/',
            name: 'images/[name]-[hash].[ext]'
          }
        }]
      },
      {
        test: /(\.css|.scss)$/,
        use: [
          { loader: 'style-loader' }, // creates style nodes from JS strings
          { loader: 'css-loader' }, // translates CSS into CommonJS
          { loader: 'sass-loader' } // compiles Sass to CSS
        ]
      },
      {
        test: /\.(jsx|js)?$/,
        exclude: /(node_modules)/,
        use: [{
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['env', 'react', 'stage-0'], // Transpiles JSX and ES6
            plugins: [
              'syntax-dynamic-import',
              ['lodash', { 'id': ['lodash', 'semantic-ui-react'] }]
            ],
          }
        }]
      }
    ]
  },
  plugins: [
  ],

  // Config for `webpack-dev-server` build
  devServer: {
    inline: true,
    // publicPath: '/dist/',
    contentBase: 'dist',
    historyApiFallback: true,
    watchContentBase: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
      ignored: /node_modules/
    },
    compress: true,
    host: '0.0.0.0', // make devServer publicly accessible within VM
    port: 3000,
    // public: 'localhost:3000'
  }
};

module.exports = config;