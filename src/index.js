import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './components/App.jsx';

import styles from './styles/app.scss';
import transitionStyles from './styles/transitions.scss';

render(
  <Router>
    <Route render={({ location }) => <App location={location} />} />
  </Router>,
  document.getElementById('root')
);
