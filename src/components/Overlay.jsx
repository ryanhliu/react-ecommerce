import React from 'react';
import { Link } from 'react-router-dom'
import { Button } from 'semantic-ui-react';
import overlayImage from '../images/overlay.png';
import styles from '../styles/overlay.scss';

class Overlay extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    return (
      <div className="overlay">
        <div className="overlay-box">
          <h1>Welcome</h1>
          <p>React Music Store written in React</p>
          <Button size="big">
            <Link to="/home">Enter</Link>
          </Button>
        </div>
      </div>
    );
  };
};

export default Overlay;
