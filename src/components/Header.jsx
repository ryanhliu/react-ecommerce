import React from 'react';
import { Link } from "react-router-dom";
import { Button, Transition, Menu, Search, Icon, Visibility } from 'semantic-ui-react';
import styles from '../styles/header.scss';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuIsFixed: false
    };
  };

  render() {
    const { location } = this.props;
    const { menuIsFixed } = this.state;

    const path = location.pathname;
    const segment = path.split('/')[1] || 'root';

    const navbarLink = (linkName) => (
      <Menu.Item as={Link}
        key={`navbar-link-${linkName}`}
        name={linkName}
        active={segment === linkName}
        to={`/${linkName}`}
        className="navbar-link"
      />
    )

    return (
      <div className="header">
        <Visibility
          once={false}
          onBottomPassed={() => this.setState({ menuIsFixed: true })}
          onBottomVisible={() => this.setState({ menuIsFixed: false })}
        >
          <Transition transitionOnMount animation="fade down" duration={1400}>
            <Menu pointing secondary borderless fixed={menuIsFixed ? 'top' : null} className="navbar">
              <Menu.Header className="navbar-title">REACT Music Store</Menu.Header>
              <Menu.Menu className="navbar-link-group">
                {[
                  navbarLink('home'),
                  navbarLink('shop'),
                  navbarLink('register'),
                  navbarLink('faq'),
                  navbarLink('blog'),
                ]}
              </Menu.Menu>
              <Menu.Item className="navbar-misc">
                <Search placeholder="Search" className="navbar-search" />
                <Button circular icon="shopping cart" size="huge" className="navbar-cart" />
              </Menu.Item>
            </Menu>
          </Transition>
        </Visibility>
      </div>
    );
  };
};

export default Header;
