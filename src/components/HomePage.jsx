import React from 'react';
import {  } from 'semantic-ui-react'

class HomePage extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    const { location } = this.props;

    const path = location.pathname;
    const segment = path.split('/')[1] || 'root';

    return (
      <div className="homepage">
        <div className="content">
          <div className="section">
          </div>
          <div className="section">
          </div>
        </div>
      </div>
    );
  };
};

export default HomePage;
