import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import Header from './Header.jsx';
import Overlay from './Overlay.jsx';
import HomePage from './HomePage.jsx';
import ShopPage from './ShopPage.jsx';
import RegisterPage from './RegisterPage.jsx';
import FaqPage from './FaqPage.jsx';
import BlogPage from './BlogPage.jsx';

const PAGES = [ 'home', 'shop', 'register', 'faq', 'blog'];

class App extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    const { location } = this.props;
    const cookieApproved = false;

    const path = location.pathname;
    const segment = path.split('/')[1] || 'root';

    return (
      <div className="app">
        <Header location={location} />
        <Route path="/" exact render={() => (
          cookieApproved
            ? <Redirect to="/home" />
            : <Redirect to="/welcome" />
        )} />
        <div className={`content background-${segment}`}>
          <TransitionGroup>
            <CSSTransition
              key={location.key}
              classNames={segment === 'welcome' ? 'overlay' : 'fadeleft'}
              appear
              in
              timeout={1000}
            >
              <Switch location={location}>
                <Route path="/welcome" exact render={(props) => <Overlay />} />
                <Route path="/home" exact render={(props) => <HomePage location={props.location} />} />
                <Route path="/shop" exact render={(props) => <ShopPage location={props.location} />} />
                <Route path="/register" exact render={(props) => <RegisterPage location={props.location} />} />
                <Route path="/faq" exact render={(props) => <FaqPage location={props.location} />} />
                <Route path="/blog" exact render={(props) => <BlogPage location={props.location} />} />
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        </div>
      </div>
    );
  };
};

export default App;
