# Ecommerce platform in React

## Set up local with Vagrant
- Run `vagrant up` to start/provision the VM
- Run `vagrant ssh` to ssh into the VM (then run `npm run start:dev` to run server)
- Or run `vagrant ssh -c 'npm run start:dev'` to run server from host machine directly
- App accessible via `localhost:3000`
