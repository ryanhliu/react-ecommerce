# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
$enable_serial_logging = false
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/trusty64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 3000, host: 3000

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
  config.vm.synced_folder ".", "/vagrant"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  config.vm.provision "file", source: "~/.gitconfig", destination: "~/.gitconfig" if File.exist?(ENV['HOME'] + '/.gitconfig')
  config.vm.provision "file", source: "~/.aliases", destination: "~/.bash_aliases" if File.exist?(ENV['HOME'] + '/.aliases')
  config.vm.provision "file", source: "~/.vimrc", destination: "~/.vimrc" if File.exist?(ENV['HOME'] + '/.vimrc')

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL

    echo "Adding NodeSource dist repos..."
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

    echo "Adding Yarn's pkg repo..."
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

    echo "Adding MongoDB pkg repo"
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
    echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

    export DEBIAN_FRONTEND=noninteractive
    apt-get update
    apt-get -y install build-essential python
    apt-get -y install git
    apt-get -y install nodejs
    apt-get -y install yarn
    apt-get -y install mongodb-org

    # Make public/build directories if doesn't exist
    # if ! [ -L /var/www ]; then
    #   rm -rf /var/www
    #   ln -fs /vagrant/public /var/www
    # fi

    # Packages need to be tuned for the VM
    rm -rf /vagrant/node_modules
    cd /vagrant && yarn install

    # Go to synced folder on `vagrant ssh`
    if ! grep -q "cd /vagrant" /home/vagrant/.bashrc ; then
      echo "cd /vagrant" >> /home/vagrant/.bashrc
    fi
  SHELL
end
